;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;; Copyright © 2019 Kevin Hua <boogerlad@gmail.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nonfree packages linux)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system trivial)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages linux)
  #:use-module (srfi srfi-1))

(define-public linux-nonfree
  (package
    (inherit linux-libre)
    (name "linux-nonfree")
    (version "5.1.1")
    ;; To follow linux-libre version, we can use the following, but this will
    ;; break if we don't immediately update the checksum.  (version
    ;; (package-version linux-libre))
    (source
     (origin
      (method url-fetch)
      (uri
       (string-append
        "https://cdn.kernel.org/pub/linux/kernel/v"
        (version-major version)
        ".x/linux-" version ".tar.xz"))
      (sha256
       (base32
        "1pcd0npnrjbc01rzmm58gh135w9nm5mf649asqlw50772qa9jkd0"))))
    (home-page "https://www.kernel.org/")
    (synopsis "Vanilla Linux kernel")
    (description "Vanilla Linux kernel which allows for non-free kernel modules / firmware.")
    ;; To build a custom kernel, pass it an alternate "kconfig":
    ;; (native-inputs
    ;;  `(("kconfig" ,(local-file "./linux-laptop.conf"))
    ;;    ,@(alist-delete "kconfig" (package-native-inputs linux-libre))))
    ))

(define-public linux-firmware
  (let* ((commit "92e17d0dd2437140fab044ae62baf69b35d7d1fa")
        (version (git-version "20190502" "1" commit)))
    (package
     (name "linux-firmware")
     (version version)
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
        (url "git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git")
        (commit commit)))
       (file-name (git-file-name "linux-firmware" version))
       (sha256 (base32 "1bsgp124jhs9bbjjq0fzmdsziwx1y5aivkgpj8v56ar0y2zmrw2d"))))
     (build-system trivial-build-system)
     (inputs `(("linux" ,linux-nonfree)))
     (outputs '("out" "iwlwifi" "amdgpu" "nvidia"))
     (arguments
      `(#:modules ((guix build utils))
        #:builder
          (let ((source (assoc-ref %build-inputs "source"))
                (fw-dir "/lib/firmware"))
            (use-modules (guix build utils))
            (for-each (lambda (p)
              (define key (car p))
              (define value (cdr p))
              (define outdir (string-append value fw-dir))
              (cond
                ((string=? key "out") (begin
                  (mkdir-p outdir)
                  (copy-recursively source outdir)))
                ((string=? key "iwlwifi") (begin
                  (mkdir-p outdir)
                  (for-each (lambda (file)
                    (copy-file file (string-append outdir "/" (basename file))))
                  (find-files source "iwlwifi-.*\\.ucode$|LICEN[CS]E\\.iwlwifi_firmware$"))))
                (else (let ((specific (string-append outdir "/" key)))
                  (mkdir-p specific)
                  (copy-recursively (string-append source "/" key) specific)
                  (for-each (lambda (file)
                    (copy-file file (string-append outdir "/" (basename file))))
                  (find-files source (string-append "LICEN[CS]E\\." key "$")))))))
            %outputs)
            #t)))
     (home-page "")
     (synopsis "Non-free firmware for the Linux kernel")
     (description "This is a collection of non-free firmware for the Linux
kernel.  This can be useful to test if you need one of them to run a specific
piece of hardware.  This package is not recommended for every-day use: The
preferred way is to acquire free, open hardware of course.  Should this be ruled
out, consider a more specific output that targets only a single
piece of hardware (e.g. #~#$linux-firmware:amdgpu).")
     (license #f))))
